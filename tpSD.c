#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "binary_heap.h"

/**
 * Reading and writing data
 */
void read_data(FILE *datain, int **dataout, int *n, int *k) {
  int *data;

  fscanf(datain, "%d", n);
  fscanf(datain, "%d", k);
  *dataout = (int *)malloc((*n) * sizeof(int));
  data = *dataout;

  for (int i = 0; i < *n; ++i, ++data) fscanf(datain, "%d", data);
}

void print_data(int *tableau, int taille) {
  for (int i = 0; i < taille; ++i) printf("%d ", tableau[i]);
  printf("\n");
}

/**
 * Exchange values between two memory space
 */
void permut(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

/* Write your functions there*/

void tri_a_bulle(int *array, size_t n, int k) {
  int i, j, tmp;
  for (i = 0; i < k; i++) {
    for (j = 1; j < n - i; j++) {
      if (array[j - 1] > array[j]) {
        permut(&(array[j - 1]), &(array[j]));
      }
    }
  }
}

void timing0(int *data, int n, int k) {
  double duree_ms;
  clock_t duree;

  duree = clock();
  heap h_build_heap = build_heap(data, n);
  duree = clock() - duree;
  duree_ms = duree / (double)CLOCKS_PER_SEC * 1000;
  printf("fait en %.7fms avec %d operation(s)\n", duree_ms, h_build_heap->op);

  duree = clock();
  heap h_add = create_binary_heap(n + 5);
  int i;
  for (i = 0; i < n; i++) add(h_add, data[i]);
  duree = clock() - duree;
  duree_ms = duree / (double)CLOCKS_PER_SEC * 1000;
  printf("fait en %.7fms avec %d operation(s)\n", duree_ms, h_add->op);

  free_heap(h_build_heap);
  free_heap(h_add);
}
void remove_higher(int *data, int n, int k, int *dest) {
  heap h;
  int i;
  if (data && dest) {
    h = build_heap(data, n);
    if (h) {
      for (i = 0; i < k; i++) {
        rem(h, &(dest[i]));
      }
      free_heap(h);
    }
  }
}

void timing1(int *data, int n, int k) {
  int i;
  double duree_ms;
  clock_t duree;
  int *get_k;
	
  duree = clock();
  get_k = malloc(sizeof(int) * k);
  remove_higher(data, n, k, get_k);
  print_data(get_k, k);
  duree = clock() - duree;
  duree_ms = duree / (double)CLOCKS_PER_SEC * 1000;
  printf("fait en %.7fms\n", duree_ms);
	/*
  duree = clock();
  tri_a_bulle(data, n, k);
  for (i = n - k; i < n; i++) {
    printf("%d ", data[i]);
  }
  printf("\n");
  duree = clock() - duree;
  duree_ms = duree / (double)CLOCKS_PER_SEC * 1000;
  printf("fait en %.7fms\n", duree_ms);
	*/
}

int compare_ints(const void *p, const void *q) {
  int x = *(const int *)p;
  int y = *(const int *)q;

  /* Avoid return x - y, which can cause undefined behaviour
     because of signed integer overflow. */
  if (x < y)
    return -1;  // Return -1 if you want ascending, 1 if you want descending
                // order.
  else if (x > y)
    return 1;  // Return 1 if you want ascending, -1 if you want descending
               // order.

  return 0;
}

/* Sort an array of n integers, pointed to by a. */
void sort_ints(int *a, size_t n) { qsort(a, n, sizeof *a, compare_ints); }

void timing2(int *data, int n, int k) {
  int i;
  double duree_ms;
  clock_t duree;
  int *get_k;
	
  duree = clock();
  get_k = heap_sort(data, n);
  for (i = n - k; i < n; i++) {
    printf("%d ", get_k[i]);
  }
  printf("\n");
  duree = clock() - duree;
  duree_ms = duree / (double)CLOCKS_PER_SEC * 1000;
  printf("fait en %.7fms\n", duree_ms);
	
  duree = clock();
  sort_ints(data, n);
  for (i = n - k; i < n; i++) {
    printf("%d ", data[i]);
  }
  printf("\n");
  duree = clock() - duree;
  duree_ms = duree / (double)CLOCKS_PER_SEC * 1000;
  printf("fait en %.7fms\n", duree_ms);
	
}

/* Main Program*/
int main(int argc, char **argv) {
  int *data;
  int n, k;
  FILE *f_in;

  if (argc > 1)
    f_in = fopen(argv[1], "r");
  else
    f_in = stdin;

  /* read the data from the file given in arg or from stdin */
  read_data(f_in, &data, &n, &k);
  //timing0(data, n, k);
  //timing1(data, n, k);
  timing2(data, n, k);
  free(data);
  return 0;
}
