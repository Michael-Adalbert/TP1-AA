#include <stdlib.h>
#include <string.h>

#include "binary_heap.h"

// acces to node s
int parent(int index) { return (index >> 1); }
int left(int index) { return ((index + 1) * 2) - 1; }
int right(int index) { return ((index + 1) * 2); }
void swap(int *a, int *b) {
  int tmp;
  tmp = *a;
  (*a) = (*b);
  (*b) = tmp;
}
void percolate_up(heap h, int index) {
  if (h) {
    if (index > 0) {
      int p_index;
      for (p_index = parent(index);
           p_index > 0 && h->tab[index] > h->tab[parent(index)];
           index = p_index, p_index = parent(index)) {
        h->op++;
        swap(&(h->tab[index]), &(h->tab[p_index]));
      }
    }
  }
}
void percolate_down(heap h, int index) {
  if (h) {
    int l, r, m;
    l = left(index);
    r = right(index);
    if (l <= h->size && h->tab[index] < h->tab[l])
      m = l;
    else
      m = index;
    if (r <= h->size && h->tab[m] < h->tab[r]) m = r;
    if (m != index) {
      h->op++;
      swap(&(h->tab[index]), &(h->tab[m]));
      percolate_down(h, m);
    }
  }
}

heap create_binary_heap(int length) {
  heap h;
  if (h = malloc(sizeof(heap))) {
    if (length > 0) {
      h->tab = malloc(sizeof(int) * length);
      if (h->tab) {
        h->length = length;
        h->size = 0;
        h->op = 0;
        return h;
      }
      free(h);
    }
  }
  return NULL;
}
heap build_heap(int *array, int size) {
  if (array) {
    heap h;
    if (h = malloc(sizeof(heap))) {
      h->tab = array;
      h->size = size;
      int i;
      for (i = (size / 2); i >= 0; i--) {
        percolate_down(h, i);
      }
      return h;
    }
    free_heap(h);
  }
  return NULL;
}
void free_heap(heap h) {
  if (h) {
    if (h->tab) {
      free(h->tab);
    }
    free(h);
  }
}
void add(heap h, int n) {
  if (h) {
    if (h->size < h->length) {
      h->tab[h->size] = n;
      h->size++;
      percolate_up(h, h->size - 1);
    }
  }
}
void rem(heap h, int *res) {
  if (h) {
    if (h->size > 0) {
      *res = h->tab[0];
      h->tab[0] = h->tab[h->size - 1];
      h->tab[h->size - 1] = 0;
      h->size--;
      percolate_down(h, 0);
    }
  }
}
void rembis(heap h) {
  swap(&(h->tab[0]), &(h->tab[h->size - 1]));
  h->size--;
  percolate_down(h, 0);
}

extern int *heap_sort(int *array, int size) {
  heap h = build_heap(array, size);
  if (h) {
    while (h->size > 0) {
      rembis(h);
    }
  }
  return h->tab;
}