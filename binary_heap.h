#ifndef __BINARY_HEAP_H__
#define __BINARY_HEAP_H__

typedef struct
{
    int size, length;
    int *tab;
    int op;
} binary_heap;

typedef binary_heap *heap;

extern heap create_binary_heap(int length);
extern heap build_heap(int *array, int size);
extern void free_heap(heap h);

extern void add(heap h, int n);
extern void rem (heap h,int* res);

extern int * heap_sort(int *array,int size);
#endif